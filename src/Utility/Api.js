import axios from "axios";

const url =
  "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe";

class Api {
  async get(link) {
    let res = await axios.get(url + link);
    return await res.data;
  }
}

const Rest = new Api();
export default Rest;
