import React from "react";
import PrimaryRouter from "./Components/PrimaryRouter";
import "./App.css";

const App = () => {
  return (
    <>
    <PrimaryRouter/>
    </>
  );
};

export default App;
