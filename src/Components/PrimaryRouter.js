import React, { useState, useEffect, lazy, Suspense } from "react";
import { Route, Switch, Link } from "react-router-dom";
import Rest from "../Utility/Api";
import "./components.css";
import Fluctuating from "./FluctuatingComponent/Fluctuating";
import HighMargin from "./MarginComponents/HighMargin";
import LowMarginComponent from "./MarginComponents/LowMarginComponent";

const AllRecipes = lazy(
  () =>
    new Promise((resolve, reject) =>
      setTimeout(() => resolve(import("./ListComponent/AllRecipes")), 400)
    )
);

const Incorrect = lazy(
  () =>
    new Promise((resolve, reject) =>
      setTimeout(() => resolve(import("./ListComponent/Incorrect")), 400)
    )
);

const Untagged = lazy(
  () =>
    new Promise((resolve, reject) =>
      setTimeout(() => resolve(import("./ListComponent/Untagged")), 400)
    )
);

const Disabled = lazy(
  () =>
    new Promise((resolve, reject) =>
      setTimeout(() => resolve(import("./ListComponent/Disabled")), 400)
    )
);
const Pages = ({ name, activeState }) => {
  return <li className={activeState === name ? "active" : null}>{name}</li>;
};

const PrimaryRouter = () => {
  const [activeState, setActiveState] = useState("ALL RECIPE(S)");
  const [recipe, setRecipe] = useState([]);
  const [page, setPage] = useState(1);
  const [highMarginSet, setHighMarginSet] = useState([]);
  const [lowMarginSet, setLowMarginSet] = useState([]);
  const [fluctuate,setFluctuate]=useState([]);

  useEffect(() => {
    Rest.get(`/recipes/?page=${page}`)
      .then((data) => {
        setRecipe(data.results);
      })
      .then((error) => {
        console.log(error);
      });

    Rest.get(`/margin-group/?order=top`)
      .then((data) => {
        setHighMarginSet(data.results);
      })
      .then((error) => {
        console.log(error);
      });
    Rest.get(`/margin-group/?order=bottom`)
      .then((data) => {
        setLowMarginSet(data.results);
      })
      .then((error) => {
        console.log(error);
      });
      Rest.get(`/fluctuation-group/?order=top`)
      .then((data) => {
        setFluctuate(data.results);
      })
      .then((error) => {
        console.log(error);
      });
  }, [page]);

  return (
    <div>
      <div className="margin-components">
        <HighMargin highMarginSet={highMarginSet} />
        <LowMarginComponent lowMarginSet={lowMarginSet} />
        <Fluctuating fluctuate={fluctuate}/>
      </div>
      <nav>
        <ul>
          <Link to="/" onClick={() => setActiveState("ALL RECIPE(S)")}>
            <Pages name={"ALL RECIPE(S)"} activeState={activeState} />
          </Link>
          <Link to="/incorrect" onClick={() => setActiveState("INCORRECT")}>
            <Pages name={"INCORRECT"} activeState={activeState} />
          </Link>
          <Link to="/untagged" onClick={() => setActiveState("UNTAGGED")}>
            <Pages name={"UNTAGGED"} activeState={activeState} />
          </Link>
          <Link to="/disabled" onClick={() => setActiveState("DISABLED")}>
            <Pages name={"DISABLED"} activeState={activeState} />
          </Link>
        </ul>
      </nav>
      <Switch>
        <Route
          exact
          path="/"
          render={() => (
            <Suspense fallback={<div>Loading...</div>}>
              <AllRecipes recipe={recipe} setPage={setPage} />
            </Suspense>
          )}
        />
        <Route
          exact
          path="/incorrect"
          component={() => (
            <Suspense fallback={<div>Loading...</div>}>
              <Incorrect recipe={recipe} setPage={setPage} />
            </Suspense>
          )}
        />
        <Route
          exact
          path="/untagged"
          component={() => (
            <Suspense fallback={<div>Loading...</div>}>
              {" "}
              <Untagged recipe={recipe} setPage={setPage} />
            </Suspense>
          )}
        />
        <Route
          exact
          path="/disabled"
          component={() => (
            <Suspense fallback={<div>Loading...</div>}>
              <Disabled recipe={recipe} setPage={setPage} />
            </Suspense>
          )}
        />
      </Switch>
    </div>
  );
};

export default PrimaryRouter;
