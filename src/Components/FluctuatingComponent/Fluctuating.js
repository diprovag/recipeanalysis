import React from "react";
import { ArrowUpOutlined } from "@ant-design/icons";
import { ArrowDownOutlined } from "@ant-design/icons";

const Fluctuating = ({ fluctuate }) => {
  console.log(fluctuate);
  return (
    <div className="highmargincomponent">
      <h2>High Margin Recipes</h2>
      <div className="content-holder container">
        {fluctuate.map((item, index) => {
          return (
            <div key={index}>
              <h4>{item.name}</h4>
              <h3>
                {item.fluctuation}
                {item.fluctuation > 40 ? (
                  <ArrowUpOutlined style={{ color: "green" }} />
                ) : (
                  <ArrowDownOutlined style={{ color: "red" }} />
                )}
              </h3>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Fluctuating;
