import React, { useState } from "react";

const AllRecipes = ({ recipe, setPage, incorrect, setIncorrect }) => {
  const [read, setRead] = useState(false);

  const visitedLink = (menu) => {
    console.log(menu);
  };

  const selectAll = () => {
    setRead(true);
  };

  return (
    <div>
      <table>
        <thead>
          <tr>
            <th>
              {" "}
              <input type="checkbox" onClick={() => selectAll()} />
            </th>
            <th>NAME</th>
            <th>LAST UPDATED</th>
            <th>COGS%</th>
            <th>COST PRICE(^)</th>
            <th>SALE PRICE</th>
            <th>GROSS MARGIN</th>
            <th>TAGS/ACTIONS</th>
          </tr>
        </thead>
        <tbody>
          {recipe.map((menu) => {
            return (
              <tr key={menu.id} className={read ? "visited" : null}>
                <td>
                  <input
                    type="checkbox"
                    onClick={(e) => visitedLink(menu)}
                    checked={read ? true : false}
                  />
                </td>

                <td>{menu.name}</td>
                <td>{menu.last_updated.date.slice(0, 10)}</td>
                <td>{menu.cogs}%</td>
                <td>{Math.ceil(menu.cost_price)}</td>
                <td>{Math.ceil(menu.sale_price)}</td>
                <td>{Math.ceil(menu.gross_margin)}%</td>
                <td>Indian Margin</td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <div className="pagination">
        <p>&laquo;</p>
        <p onClick={() => setPage(1)}>1</p>
        <p onClick={() => setPage(2)}>2</p>
        <p>&raquo;</p>
      </div>
    </div>
  );
};

export default AllRecipes;
