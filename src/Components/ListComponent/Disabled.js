import React, { useState } from "react";
import AllRecipes from "./AllRecipes";

const Disabled = ({ recipe, setPage }) => {
  const [disabled, setDisabled] = useState(false);

  let disableModifyRecipe = recipe.filter(
    (ele) => ele.is_disabled === disabled
  );

  return (
    <div>
      <div className="selection">
        <div onClick={() => setDisabled(false)}>
          <h5 disabled={disabled}>Enable</h5>
        </div>

        <div onClick={() => setDisabled(true)}>
          <h5 disabled={disabled}>Disable</h5>
        </div>
      </div>
      <AllRecipes recipe={disableModifyRecipe} setPage={setPage} />
    </div>
  );
};

export default Disabled;
