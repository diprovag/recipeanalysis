import React, { useState } from "react";
import AllRecipes from "./AllRecipes";

const Incorrect = ({ recipe, setPage }) => {
  const [incorrect, setIncorrect] = useState(false);

  let modifiedrecipe = recipe.filter((ele) => ele.is_incorrect === incorrect);

  return (
    <div>
      <div className="selection">
        <div onClick={() => setIncorrect(false)}>
          <h5 incorrect={incorrect}>Correct</h5>
        </div>

        <div onClick={() => setIncorrect(true)}>
          <h5 incorrect={incorrect}>Incorrect</h5>
        </div>
      </div>
      <AllRecipes
        recipe={modifiedrecipe}
        setPage={setPage}
        incorrect={incorrect}
        setIncorrect={setIncorrect}
      />
    </div>
  );
};

export default Incorrect;
