import React, { useState } from "react";
import AllRecipes from "./AllRecipes";

const Untagged = ({ recipe, setPage }) => {
  const [untagged, setUntagged] = useState(false);

  let tagModifyRecipe = recipe.filter((ele) => ele.is_untagged === untagged);
  return (
    <div>
      <div className="selection">
        <div onClick={() => setUntagged(false)}>
          <h5 untagged={untagged}>Tagged</h5>
        </div>

        <div onClick={() => setUntagged(true)}>
          <h5 untagged={untagged}>Untagged</h5>
        </div>
      </div>
      <AllRecipes recipe={tagModifyRecipe} setPage={setPage} />
    </div>
  );
};

export default Untagged;
