import React from "react";
import { Progress } from "antd";
import "antd/dist/antd.css";

const LowMarginComponent = ({ lowMarginSet }) => {
  return (
    <div className="highmargincomponent">
      <h2>Low Margin Recipes</h2>
      <div className="content-holder container">
        {lowMarginSet.map((item, index) => {
          return (
            <div key={index}>
              <h4>{item.name}</h4>
              <p>
                {" "}
                <Progress
                  type="circle"
                  percent={item.margin}
                  width={60}
                  strokeColor="red"
                />
              </p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default LowMarginComponent;
