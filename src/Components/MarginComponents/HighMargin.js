import React from "react";
import { Progress } from "antd";
import "antd/dist/antd.css";

const HighMargin = ({ highMarginSet }) => {
  return (
    <div className="highmargincomponent">
      <h2>High Margin Recipes</h2>
      <div className="content-holder container">
        {highMarginSet.map((item,index) => {
          return (
            <div key={index}>
              <h4>{item.name}</h4>
             <p> <Progress type="circle" percent={item.margin} width={60} /></p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default HighMargin;
